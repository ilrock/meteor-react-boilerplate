import React from 'react';
import { Accounts } from 'meteor/accounts-base';
import PropTypes from 'prop-types';

const PrivateHeader = (props) => {
  return(
    <nav>
      <div className="nav-wrapper">
        <a href="#" className="brand-logo"> {props.title} </a>
        <ul id="nav-mobile" className="right hide-on-med-and-down">
          <li><a href="#" onClick={(e) => {
            e.preventDefault();
            Accounts.logout()
          }}> Logout </a></li>
        </ul>
      </div>
    </nav>
  )
  // return (
  //   <div>
  //     <h1>{props.title}</h1>
  //     <button onClick={() => Accounts.logout() }>Logout</button>
  //   </div>
  // )
};

PrivateHeader.propTypes = {
  title: PropTypes.string.isRequired
};

export default PrivateHeader;