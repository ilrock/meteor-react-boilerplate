import React from 'react';
import { Link } from 'react-router';
import { Accounts } from 'meteor/accounts-base';

export default class Signup extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            error: ""
        };
    }
    render(){
        return(
            <div className="container">
                <h1> Join </h1>
                {this.state.error ? <p>{this.state.error}</p> : undefined}
                <form>
                    <input type="email" ref="email" name="email" placeholder="Email"/>
                    <input type="password" ref="password" name="password" placeholder="Password"/>
                    <button onClick={this.onSubmit.bind(this)}> Create Account </button>
                </form>
                <Link to="/"> Have an account? </Link>
            </div>
            
        );
    }

    onSubmit(e){
        e.preventDefault();

        Accounts.createUser({
            email: this.refs.email.value.trim(),
            password: this.refs.password.value.trim()
        }, (err) => {
            if (err){
                this.setState({ error: err.reason })
            } else{
                this.setState({ error: "" })
            }
        });
    }
}