import React from 'react';
import { Link } from 'react-router';
import { Meteor } from 'meteor/meteor';

export default class Login extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            error: ""
        };
    }

    render(){
        return(
            <div className="container">
                <h1> Login </h1>
                {this.state.error ? <p>{this.state.error}</p> : undefined}                
                <form>
                    <input type="email" ref="email" name="email" placeholder="Email"/>
                    <input type="password" ref="password" name="password" placeholder="Password"/>
                    <button onClick={this.onSubmit.bind(this)}> Login </button>
                </form>
                <Link to="/signup"> Don't have an account? </Link>
            </div>
        );
    }

    onSubmit(e){
        e.preventDefault();

        Meteor.loginWithPassword(this.refs.email.value, this.refs.password.value, (err) => {
            if (err){
                this.setState({ error: err.reason })
            } else{
                this.setState({ error: "" })
            }
        })
    }
}